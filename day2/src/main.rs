use aoc_utils;

fn main() {
    let data = aoc_utils::fetch_data_by_env(2);
    let data = format_data(data);
    let data = calculate_own_move(data);
    let result:u32 = data.iter().map(|item| {(item.1+item.2)  as u32}).sum();
    println!("{result}");
}

fn format_data(data: String) -> Vec<(u8, u8)> {
    let mut moves = Vec::new();
    for line in data.lines() {
        let pair = line.split_whitespace().collect::<Vec<&str>>();
        let opponent_move = match pair.get(0).unwrap().as_ref() {
            "A" => 1,
            "B" => 2,
            "C" => 3,
            _ => {
                let string = pair.get(0).unwrap();
                panic!("invalid move {string}");
            }
        };
        let own_move = match pair.get(1).unwrap().as_ref() {
            "X" => 0,
            "Y" => 3,
            "Z" => 6,
            _ => {
                let string = pair.get(1).unwrap();
                panic!("invalid move {string}");
            }
        };
        moves.push((opponent_move, own_move))
    }
    return moves;
}

fn calculate_own_move(data: Vec<(u8, u8)>) -> Vec<(u8, u8, u8)> {
    data.iter().map(|item| {
        (
            item.0,
            item.1,
            if item.1 == 3 {
                item.0
            } else if item.0 == 1 && item.1 == 6 {
                2
            } else if item.0 == 2 && item.1 == 6 {
                3
            } else if item.0 == 3 && item.1 == 6 {
                1
            } else if item.0 == 1 {
                3
            }else if item.0 == 2 {
                1
            }else {
                2
            }
        )
    }).collect()
}
