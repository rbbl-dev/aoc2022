use std::collections::{HashMap, HashSet};

fn main() {
    let data = aoc_utils::fetch_data_by_env(6);
    let data = get_marker_point(&data);
    println!("{}", data.get(0).unwrap())
}

fn get_marker_point(data: &str) -> Vec<usize> {
    let mut result = Vec::new();
    let data: Vec<char> = data.chars().collect();
    for i in 0..data.len() - 13 {
        let chars = data[i..=i + 13].to_vec();
        if HashSet::<&char>::from_iter(chars.iter()).len() == chars.len() {
            result.push(i + 14);
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let result = get_marker_point("mjqjpqmgbljsphdztnvjfqwrcgsmlb");
        assert_eq!(19, result[0])
    }
    #[test]
    fn test2() {
        let result = get_marker_point("bvwbjplbgvbhsrlpgdmjqwftvncz");
        assert_eq!(23, result[0])
    }
    #[test]
    fn test3() {
        let result = get_marker_point("nppdvjthqldpwncqszvftbrmjlhg");
        assert_eq!(23, result[0])
    }
    #[test]
    fn test4() {
        let result = get_marker_point("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg");
        assert_eq!(29, result[0])
    }
    #[test]
    fn test5() {
        let result = get_marker_point("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw");
        assert_eq!(26, result[0])
    }
}
