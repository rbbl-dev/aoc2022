fn main() {
    let data = aoc_utils::fetch_data_by_env(3);
    let result: u32 = data
        .lines()
        .collect::<Vec<&str>>()
        .chunks(3)
        .map(|pouches| group_to_duplicate_item(pouches))
        .map(|item| item_to_priority(item))
        .sum();
    println!("{result}")
}

fn group_to_duplicate_item(group: &[&str]) -> char {
    for char_a in group.get(0).unwrap().chars() {
        for char_b in group.get(1).unwrap().chars() {
            for char_c in group.get(2).unwrap().chars() {
                if char_a == char_b && char_b == char_c {
                    return char_a;
                }
            }
        }
    }
    panic!("no duplicate found")
}

fn item_to_priority(item: char) -> u32 {
    let mut i = 0;
    for letter in 'a'..='z' {
        i += 1;
        if letter == item {
            return i;
        }
    }
    for letter in 'A'..='Z' {
        i += 1;
        if letter == item {
            return i;
        }
    }
    panic!("item '{item}' out of range")
}
