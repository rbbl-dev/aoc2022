use std::ops::RangeInclusive;

fn main() {
    let data = aoc_utils::fetch_data_by_env(4);
    let data = format_data(data);
    let mut containment_counter = 0;
    for pair in data {
        if range_overlaps_range(&pair.0, &pair.1) || range_overlaps_range(&pair.1, &pair.0) {
            containment_counter += 1;
        }
    }
    println!("{containment_counter}")
}

fn format_data(data: String) -> Vec<(RangeInclusive<u8>, RangeInclusive<u8>)> {
    data.lines()
        .map(|line| {
            let split: Vec<&str> = line.split(',').collect();
            let range_data_1: Vec<u8> = split
                .get(0)
                .unwrap()
                .split('-')
                .map(|string| string.parse().unwrap())
                .collect();
            let range_data_2: Vec<u8> = split
                .get(1)
                .unwrap()
                .split('-')
                .map(|string| string.parse().unwrap())
                .collect();
            (
                range_data_1.get(0).unwrap().clone()..=range_data_1.get(1).unwrap().clone(),
                range_data_2.get(0).unwrap().clone()..=range_data_2.get(1).unwrap().clone(),
            )
        })
        .collect()
}

fn range_overlaps_range(
    containing_range: &RangeInclusive<u8>,
    contained_range: &RangeInclusive<u8>,
) -> bool {
    containing_range.contains(contained_range.start())
        || containing_range.contains(contained_range.end())
}
