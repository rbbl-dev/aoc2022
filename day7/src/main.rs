use std::collections::HashMap;

fn main() {
    let data = aoc_utils::fetch_data_by_env(7);
    let data = parse_data(&data.trim());
    let data = calc_folder_size(&data);
    let root_size = data.get("").unwrap();
    let additionally_needed_free_space = (root_size + 30000000) - 70000000;
    let mut result: Vec<&u32> = data
        .values()
        .filter(|num| num > &&additionally_needed_free_space)
        .collect();
    result.sort();
    println!("{}", result[0])
}

fn calc_folder_size(data: &Vec<File>) -> HashMap<&str, u32> {
    let mut result = HashMap::new();
    for file in data {
        let mut path = file.path.as_str();
        while path != "" {
            path = remove_last_path_item(path);
            result.insert(
                path,
                match result.get(path) {
                    Some(val) => val + file.size,
                    None => file.size,
                },
            );
        }
    }
    result
}

fn parse_data(data: &str) -> Vec<File> {
    let mut result = Vec::new();
    let mut current_path = String::new();
    for line in data.lines() {
        if line.starts_with("$ ls") || line.starts_with("dir") {
            continue;
        } else if line.starts_with("$ cd ") {
            let cd_to = line.trim_start_matches("$ cd ");
            current_path = if cd_to == ".." {
                remove_last_path_item(&current_path).to_string()
            } else {
                if cd_to == "/" {
                    cd_to.to_string()
                } else if current_path.ends_with('/') {
                    String::from(&current_path) + &cd_to
                } else {
                    String::from(&current_path) + "/" + &cd_to
                }
            }
        } else {
            let size_and_file = line.split_whitespace().collect::<Vec<&str>>();
            let file_path = current_path.clone();
            let mut file_path = if file_path != "/" {
                file_path + "/"
            } else {
                file_path
            };
            file_path.push_str(size_and_file.get(1).unwrap());
            let file = File {
                path: file_path,
                size: size_and_file.get(0).unwrap().parse().unwrap(),
            };
            result.push(file);
        }
    }
    result
}

fn remove_last_path_item(input: &str) -> &str {
    let last_slash = input.len() - input.chars().rev().collect::<String>().find("/").unwrap() - 1;
    &input[..last_slash]
}

struct File {
    path: String,
    size: u32,
}
