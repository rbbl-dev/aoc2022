fn main() {
    let data = aoc_utils::fetch_data_by_env(5);
    let data = parse_data(&data);
    let mut field = data.0;
    for instruction in data.1 {
        field = apply_instruction(&field, instruction);
        for column in &field {
            for item in column {
                print!("{item}");
            }
            println!();
        }
        println!();
    }
    for col in field {
        print!("{}", col.last().unwrap());
    }
}

fn apply_instruction(field: &Vec<Vec<char>>, instruction: MoveInstruction) -> Vec<Vec<char>> {
    let from_column = field.get((instruction.from - 1) as usize).unwrap();
    let mut field = field.clone();
    field
        .get_mut((instruction.to - 1) as usize)
        .unwrap()
        .append(
            from_column[from_column.len() - (instruction.amount as usize)..]
                .iter()
                .map(|ch| ch.clone())
                .collect::<Vec<char>>()
                .as_mut(),
        );
    let from_column = std::mem::replace(
        field.get_mut((instruction.from - 1) as usize).unwrap(),
        from_column[..from_column.len() - (instruction.amount as usize)]
            .iter()
            .map(|ch| ch.clone())
            .collect::<Vec<char>>(),
    );
    field
}

fn parse_data(data: &str) -> (Vec<Vec<char>>, Vec<MoveInstruction>) {
    let data = data.split_at(data.find("move").unwrap());
    let field = parse_field(data.0);
    let instructions = parse_move_instructions(data.1);
    return (field, instructions);
}

fn parse_field(field: &str) -> Vec<Vec<char>> {
    let field = field.trim_end();
    let mut colum_indices: Vec<usize> = Vec::new();
    for (index, character) in field.lines().last().unwrap().chars().enumerate() {
        if character.is_numeric() {
            colum_indices.push(index)
        }
    }
    let lines: Vec<&str> = field.lines().collect();
    let mut field: Vec<Vec<char>> = Vec::new();
    for _ in &colum_indices {
        field.push(Vec::new());
    }
    for (index, line) in lines.iter().enumerate() {
        if index < lines.len() {
            let mut column_count = 0;
            for (index, character) in line.chars().enumerate() {
                if colum_indices.contains(&index) {
                    if character.is_alphabetic() {
                        field.get_mut(column_count).unwrap().push(character);
                    }
                    column_count += 1;
                }
            }
        }
    }
    for column in field.iter_mut() {
        column.reverse();
    }
    return field;
}

fn parse_move_instructions(data: &str) -> Vec<MoveInstruction> {
    let mut result = Vec::new();
    for line in data.trim().lines() {
        let mut num_count = 0;
        let mut from = 0;
        let mut to = 0;
        let mut amount = 0;
        for word in line.split_whitespace() {
            match word.parse::<u32>() {
                Ok(num) => {
                    match num_count {
                        0 => amount = num,
                        1 => from = num,
                        2 => to = num,
                        _ => {}
                    };
                    num_count += 1;
                }
                Err(_) => {}
            }
        }
        result.push(MoveInstruction { to, from, amount })
    }
    return result;
}

struct MoveInstruction {
    from: u32,
    to: u32,
    amount: u32,
}
