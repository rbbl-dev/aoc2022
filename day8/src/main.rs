fn main() {
    let data = aoc_utils::fetch_data_by_env(8);
    let data = parse_data(&data);
    let result = calc_max_score(&data);
    println!("{result}");
}

fn calc_max_score(data: &Vec<Vec<u8>>) -> u32 {
    let mut max_score = 0;
    for y in 0..data.len() {
        for x in 0..data[y].len() {
            let score = calc_score(x, y, data);
            if score > max_score {
                max_score = score
            }
        }
    }
    max_score
}

fn calc_score(x: usize, y: usize, data: &Vec<Vec<u8>>) -> u32 {
    let point_value = data[y][x];
    let mut w_score = 0;
    let range = 0..x;
    for x in range.rev() {
        w_score += 1;
        if data[y][x] >= point_value {
            break;
        }
    }
    let mut e_score = 0;
    let range = x + 1..data[y].len();
    for x in range.clone() {
        e_score += 1;
        if data[y][x] >= point_value {
            break;
        }
    }
    let mut n_score = 0;
    let range = 0..y;
    for y in range.clone().rev() {
        n_score += 1;
        if data[y][x] >= point_value {
            break;
        }
    }
    let mut s_score = 0;
    let range = y + 1..data.len();
    for y in range.clone() {
        s_score += 1;
        if data[y][x] >= point_value {
            break;
        }
    }
    return n_score * e_score * s_score * w_score;
}

fn parse_data(data: &str) -> Vec<Vec<u8>> {
    let mut result = Vec::new();
    for line in data.lines() {
        let mut row = Vec::new();
        for ch in line.chars() {
            row.push(ch.to_digit(10).unwrap() as u8);
        }
        result.push(row);
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        let data = parse_data(
            "30373
25512
65332
33549
35390",
        );
        let result = calc_max_score(&data);
        assert_eq!(8, result)
    }
}
