use aoc_utils;

fn main() {
    let data = aoc_utils::fetch_data_by_env(1);
    let mut flat_data = flatten_data(data);
    flat_data.sort();
    let result: u32 = flat_data.iter().rev().take(3).sum();
    println!("{result}");
}

fn flatten_data(data: String) -> Vec<u32> {
    let mut flat_data = Vec::new();
    let mut current_count = 0;
    for line in data.lines() {
        match line.parse::<u32>() {
            Ok(int) => {
                current_count += int;
            }
            Err(_) => {
                flat_data.push(current_count);
                current_count = 0;
            }
        }
    }
    return flat_data;
}
