use std::env;

pub fn fetch_data(cookie_value: String, day: u8) -> String {
    let client = reqwest::blocking::Client::new();
    return client
        .get(format!("https://adventofcode.com/2022/day/{day}/input"))
        .header("Cookie", cookie_value)
        .send()
        .expect("could not fetch data")
        .text()
        .expect("failed to extract body");
}

pub fn fetch_data_by_env(day: u8) -> String {
    fetch_data(
        env::var("COOKIE_DATA").expect("you have to set the 'COOKIE_DATA' environment variable"),
        day,
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = fetch_data_by_env(1);
        println!("{result}")
    }
}
